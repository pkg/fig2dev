Source: fig2dev
Maintainer: Roland Rosenfeld <roland@debian.org>
Section: graphics
Priority: optional
Build-Depends: debhelper-compat (= 13),
               gawk,
               ghostscript <!nodoc>,
               libpng-dev,
               netpbm <!nocheck>,
               texlive-font-utils <!nodoc>,
               texlive-fonts-recommended <!nodoc>,
               texlive-lang-german <!nodoc>,
               texlive-latex-base <!nodoc>,
               texlive-latex-extra <!nocheck !nodoc>,
               texlive-latex-recommended <!nodoc>,
               texlive-pictures <!nodoc>,
               zlib1g-dev
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/debian/fig2dev
Vcs-Git: https://salsa.debian.org/debian/fig2dev.git
Homepage: https://sourceforge.net/projects/mcj/
Rules-Requires-Root: no

Package: fig2dev
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${shlibs:Depends},
         gawk,
         ghostscript,
         netpbm,
         x11-common
Suggests: xfig
Breaks: transfig (<< 1:3.2.6~beta-1~)
Provides: transfig
Replaces: transfig (<< 1:3.2.6~beta-1~)
Description: Utilities for converting XFig figure files
 This package contains utilities (mainly fig2dev) to handle XFig
 (Facility for Interactive Generation of figures) files.
 .
 It can convert files produced by xfig to box, cgm, dxf, epic, eepic,
 eepicemu, emf, eps, gbx (Gerber beta driver), gif, ibmgl, jpeg,
 latex, map (HTML image map), mf (MetaFont), mp (MetaPost), pcx, pdf,
 pdftex, pdftex_t, pic, pict2e, pictex, png, ppm, ps, pstex, pstex_t,
 pstricks, ptk (Perl/Tk), shape (LaTeX shaped paragraphs), sld
 (AutoCad slide format), svg, textyl, tiff, tikz, tk (Tcl/Tk), tpic,
 xbm and xpm.
