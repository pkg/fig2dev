Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2015-2025, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig

Files: Makefile.in
Copyright: 1994-2024, Free Software Foundation, Inc.
License: Expat~Xfig

Files: aclocal.m4
Copyright: 1996-2024, Free Software Foundation, Inc.
License: FSFULLR

Files: ar-lib
 compile
 depcomp
 missing
Copyright: 1996-2024, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 2015-2023, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig or FSFUL

Files: debian/*
Copyright: 2010, Steve Langasek <vorlon@debian.org>
 1999-2025, Roland Rosenfeld <roland@debian.org>
 1998, Paul Slootman <paul@debian.org>
 1998, Christian Meder <meder@isr.uni-stuttgart.de>
 1998, 1999, Edward Betts <edward@debian.org>
 1997, joost witteveen <joostje@debian.org>
 1997, 1998, Enrique Zanardi <ezanardi@molec1.dfis.ull.es
License: GPL-2+

Files: debian/patches/34_arc-coincident.patch
 debian/patches/35_arcradius3.patch
Copyright: 2015-2025, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig

Files: fig2dev/Makefile.in
Copyright: 1994-2024, Free Software Foundation, Inc.
License: Expat~Xfig

Files: fig2dev/alloc.h
Copyright: 2015-2023, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985, Supoj Sutantavibul
License: Expat~Xfig

Files: fig2dev/dev/Makefile.in
Copyright: 1994-2024, Free Software Foundation, Inc.
License: Expat~Xfig

Files: fig2dev/dev/genepic.c
 fig2dev/dev/genpic.c
 fig2dev/dev/gentpic.c
Copyright: 2015-2023, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1988, Conrad Kwok
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig

Files: fig2dev/dev/genlatex.c
Copyright: 2015-2023, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1988, Frank Schmuck
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig

Files: fig2dev/dev/gensvg.c
Copyright: 2015-2024, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 2002-2015, Brian V. Smith
 2002-2006, Martin Kroeker
 2002, Anthony Starks
License: Expat~Xfig

Files: fig2dev/dev/picpsfonts.h
Copyright: 2015-2019, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1992, Uri Blumenthal, IBM
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig

Files: fig2dev/dev/readgif.c
Copyright: 2015-2020, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig or NTP

Files: fig2dev/dev/readxbm.c
Copyright: 2015-2020, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig or HPND-sell-variant

Files: fig2dev/i18n/*
Copyright: 1994-2024, Free Software Foundation, Inc.
License: Expat~Xfig

Files: fig2dev/i18n/Makefile.am
Copyright: 2015-2025, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig

Files: fig2dev/iso2tex.c
Copyright: 1999, 2002, 2007, Brian V. Smith
 1992, Herbert Bauer and B. Raichle
License: Expat~Xfig

Files: fig2dev/lib/*
Copyright: 2015, 2018, Thomas Loimer <thomas.loimer@tuwien.ac.at>
License: FSFAP

Files: fig2dev/lib/getline.c
 fig2dev/lib/getline.h
 fig2dev/lib/getopt.c
Copyright: 2015-2025, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutanthavibul
License: Expat~Xfig

Files: fig2dev/lib/strdup.c
Copyright: 1999, Gene Michael Stover
License: LGPL-2.1

Files: fig2dev/tests/Makefile.in
Copyright: 1994-2024, Free Software Foundation, Inc.
License: Expat~Xfig

Files: fig2dev/trans_spline.c
 fig2dev/trans_spline.h
Copyright: 1995, C. Blanc and C. Schlick
License: Expat~Xfig

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: m4/*
Copyright: 2015, 2018, Thomas Loimer <thomas.loimer@tuwien.ac.at>
License: FSFAP

Files: man/Makefile.in
Copyright: 1994-2024, Free Software Foundation, Inc.
License: Expat~Xfig

Files: man/fig2dev.1.in
Copyright: 2015-2018, Thomas Loimer <thomas.loimer@tuwien.ac.at>
 1991, Micah Beck
 1989-2015, Brian V. Smith
 1985-1988, Supoj Sutantavibul
License: Expat-Open-Group or NTP~disclaimer

Files: man/transfig.1
Copyright: 1991-2002, Brian Smith
 1991, Micah Beck
 1985, Supoj Sutantavibul
License: Expat~Xfig

Files: transfig/Makefile.in
Copyright: 1994-2024, Free Software Foundation, Inc.
License: Expat~Xfig

Files: transfig/mkfile.c
Copyright: 1991, Micah Beck
 1985, Supoj Sutantavibul
License: Expat~Xfig

Files: fig2dev/fig2ps2tex fig2dev/fig2ps2tex.csh man/fig2ps2tex.1 man/pic2tpic.1
Copyright:
 Copyright (c) 1991-1999 by Micah Beck
 Parts Copyright (c) 1985-1988 by Supoj Sutanthavibul
 Parts Copyright (c) 1988 by Conrad Kwok
 Parts Copyright (c) 1988 by Frank Schmuck
 Parts Copyright (c) 1989-2016 by Brian V. Smith
 Parts Copyright (c) 1992 by Herbert Bauer and  B. Raichle
 Parts Copyright (c) 1992 by Uri Blumenthal, IBM
 Parts Copyright (c) 1993-2002 by Anthony Starks
 Parts Copyright (c) 1994-2002 by Thomas Merz
 Parts Copyright (c) 1995 by C. Blanc and C. Schlick
 Parts Copyright (c) 1998 by Mike Markowski
 Parts Copyright (c) 1988 by Frank Schmuck
 Parts Copyright (c) 1999 by Philippe Bekaert
 Parts Copyright (c) 1999 by T. Sato
 Parts Copyright (c) 2001 by Michael Schrick
 Parts Copyright (c) 2002 by Christian Gollwitzer (auriocus)
 Parts Copyright (c) 2002-2006 by Martin Kroeker
 Parts Copyright (c) 2004-2008 by Eugene Ressler
 Parts Copyright (c) 2014-2024 by Thomas Loimer
License: MIT~Xfig
